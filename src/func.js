const { listOfPosts2 } = require("./posts");

const getSum = (str1, str2) => {
  let n1=0,n2=0;
  if(typeof(str1)!="string" || typeof(str2)!="string"){
    return false;
  }
  if(str1.match(/[a-z]/) || str2.match(/[a-z]/)){
    return false;
  }
  if(str1.length!=0){
    n1=parseInt(str1);
  }
  if(str2.length!=0){
    n2=parseInt(str2);
  }
  
  return (n1 + n2).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let c=0,p=0;
  listOfPosts.forEach(el=>{
    if(el.author == authorName){
      p++;
    }
    if(el.comments!=undefined){
      el.comments.forEach(el2=>{
        if(el2.author==authorName){
          c++;
        }
      })
    } 
  })
  return "Post:"+p+',comments:'+c;
};

const tickets=(people)=> {
  const cost = 25;
  let cash =0; 
  let state = 'YES';
  people.forEach(el => {
      if(el>cost && cash<el-cost){ ///25,50,100
        state='NO';
        return;
      }
      if(el==25){
        cash+=el;
      }
      else{
        cash+=el-cost;
      } 
  });
  return state;
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
